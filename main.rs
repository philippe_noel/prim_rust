#[macro_use] extern crate text_io;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;


struct Sommet {
    nom: i32,
    lst_arc: Vec<Arc>,
}

struct Arc {
    destination: i32,
    poids: i32,
}

impl Sommet {
    fn new(nom: i32) -> Sommet {
        Sommet {
            nom,
            lst_arc: Vec::new(),
        }
    }

    fn add_arc(&mut self, arc: Arc) {
        &self.lst_arc.push(arc);
    }

    fn print_arc(&self) {
        println!("Sommet : {}", &self.nom);
        for ii in &self.lst_arc {
            print!("{} ", ii.destination);
        }
        println!("");
    }

    fn find_minimum_arc(&self, v: &Vec<usize>) -> (i32, i32) {
        // retourne la destination et le poids du plus faible arc
        let mut min: i32 = 99999;
        let mut destination = 0;
        for arc in &self.lst_arc {
            if !is_in(&v, arc.get_name() as usize) && arc.get_poids() < min {
                println!("desti {}", arc.get_name());
                min = arc.get_poids();
                destination = arc.get_name();
                println!("return : {}", destination);
            }
        }
        (destination, min)
    }
}

fn is_in(v: &Vec<usize>, value: usize) -> bool {
    if v.len() == 0 {
        return false
    }
    for ii in v {
        if *ii == value {
            return true
        }
    }
    return false
}

impl Arc {
    fn new(destination: i32, poids: i32) -> Arc {
        Arc {destination, poids}
    }
    fn get_poids(&self) -> i32 {
        self.poids
    }
    fn get_name(&self) -> i32 {
        self.destination
    }
}

fn generer_lst_sommet(n: i32) -> Vec<Sommet> {
    let mut lst_sommet: Vec<Sommet> = Vec::new();
    for ii in 0..n {
        lst_sommet.push(Sommet::new(ii));
    }
    lst_sommet
}

fn remove_item(v: &Vec<i32>, value: i32) -> Option<usize> {
    for (ii, aa) in v.iter().enumerate() {
        if *aa == value{
            let toto: usize = ii;
            return Some(toto)
        }
    }
    return None
}

fn prim(lst: &Vec<Sommet>) {
    //Initial edge is the first one

    let n: i32 = lst.len() as i32;

    let mut lst_non_see_edges: Vec<i32> = (1..n).collect::<Vec<i32>>();
    let mut lst_see_edges: Vec<usize> = Vec::new();

    //see A
    lst_non_see_edges.remove(0);
    let (aa, _) = lst[0].find_minimum_arc(&lst_see_edges);
    lst_non_see_edges.remove(aa as usize);
    lst_see_edges.push(aa as usize);
    println!("0 -> {}", aa);

    while lst_non_see_edges.len() != 0 {
        let mut min_edge_value = 9999;
        let mut min_edge_destination = 0;
        let mut current_edge = 0;
        for ii in &lst_see_edges {
            let (aa, bb) = lst[*ii as usize].find_minimum_arc(&lst_see_edges);
            if bb < min_edge_value {
                min_edge_value = bb;
                min_edge_destination = aa;
                current_edge = *ii;
            } 
        }
        println!("minimum edge value : {} \n {:?}", min_edge_destination, lst_non_see_edges);
        match remove_item(&lst_non_see_edges, min_edge_destination) {
            Some(t) => {
                lst_non_see_edges.remove(t);
                lst_see_edges.push(min_edge_destination as usize);
                println!("{} -> {}", current_edge, aa);
            },
            None => (),
        }

    }

}

fn main() {

    let f = File::open("graph_test.graph").expect("File nooooooo");
    let mut f = BufReader::new(f);

    let mut n_sommet = String::new();
    f.read_line(&mut n_sommet);
    let n_sommet= n_sommet
        .trim()
        .parse::<i32>()
        .unwrap();

    let mut lst_sommet: Vec<Sommet> = generer_lst_sommet(n_sommet);

    let mut s: i32;
    let mut j: i32;
    let mut i: i32;
    for line in f.lines() {
        let tmp = line.unwrap();
        scan!(tmp.bytes() => "{} {} {}", s, i, j);
        lst_sommet[(s-1) as usize].add_arc(Arc::new(j-1, i));
        lst_sommet[(j-1) as usize].add_arc(Arc::new(s-1, i));
    }
    lst_sommet[0].print_arc();
    lst_sommet[1].print_arc();
    prim(&lst_sommet);
}
